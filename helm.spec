Name:           helm
Version:        3.4.0
Release:        1%{?dist}
Summary:        The Kubernetes Package Manager.

License:        Apache-2.0
URL:            https://helm.sh
Source0:        https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  git
BuildRequires:  make
BuildRequires:  golang >= 1.13.6-1

%description
Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.
Charts are easy to create, version, share, and publish — so start using Helm and stop the copy-and-paste.

%global debug_package %{nil}

%prep
%autosetup -n %{name}-%{version}

# many golang binaries are "vendoring" (bundling) sources, so remove them. Those dependencies need to be packaged independently.
rm -rf vendor

%build
%make_build

%install
install -d %{buildroot}%{_bindir}
install -p -m 0755 ./bin/%{name} %{buildroot}%{_bindir}/%{name}

%files
%doc CONTRIBUTING.md LICENSE README.md
%{_bindir}/%{name}


%changelog
* Sun Feb  2 2020 Max Audron
- 

