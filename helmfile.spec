Name:           helmfile
Version:        0.133.0
Release:        1%{?dist}
Summary:        Deploy Kubernetes Helm Charts

License:        MIT
URL:            https://helm.sh
Source0:        https://github.com/roboll/%{name}/archive/v%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  git
BuildRequires:  make
BuildRequires:  golang >= 1.13.6-1

%description
Helmfile is a declarative spec for deploying helm charts. It lets you...

 - Keep a directory of chart value files and maintain changes in version control.
 - Apply CI/CD to configuration changes.
 - Periodically sync to avoid skew in environments.

To avoid upgrades for each iteration of helm, the helmfile executable delegates to helm - as a result, helm must be installed.

%global debug_package %{nil}

%prep
%autosetup -n %{name}-%{version}

# many golang binaries are "vendoring" (bundling) sources, so remove them. Those dependencies need to be packaged independently.
rm -rf vendor

%build
%make_build

%install
install -d %{buildroot}%{_bindir}
install -p -m 0755 ./%{name} %{buildroot}%{_bindir}/%{name}

%files
%doc CONTRIBUTING.md LICENSE README.md
%{_bindir}/%{name}


%changelog
* Sun Feb  2 2020 Max Audron
- 

